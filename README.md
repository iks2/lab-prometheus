# lab-prometheus
Prometheus 모니터링 시스템 Hands-on을 위한 Project 입니다.   
총 3개의 실습용 스크립트가 구성되어 있습니다.

- lab-01 : Prometheus 설치
- lab-02 : Prometheus 시스템에 k8s metric 연계   
- 10 폴더 : 실습문서 2.1. Prometheus 에 kubernetes 메트릭 추가 하기
- 20 폴더 : 실습문서 2.2. Prometheus 용 Service Account 추가하기  
- 30 폴더 : 실습문서 2.3. kubernetes Access 권한 할당 하기
- lab-03 : Grafana  설치


# 실습 시 주의사항
- lab-01부터 lab-03까지만 실습을 진행 합니다.
- 실습자들이 생성하는 리소스명은 중복될 수 있습니다. 이를 구분하기 위해 주어진 lab 번호를 이용 합니다. namespace 및 리소스 생성할 때 본인에게 부여된 lab 번호를 사용 합니다. 명명 규칙은 실습문서를 따릅니다.
- 각 lab 폴더 안에는 yaml 파일이 존재 합니다. 실습 단계별로 리소스명과 namespace명을 hands-on 문서에 기술된 데로 필요한 곳만 수정 합니다. 잘못 수정할 경우 다른 실습자에게 피해를 줄 수 있습니다.
- yaml파일에 있는 주석을 해제 하지 않습니다. 주석 해제 시에는 yaml은 오작동을 하게 됩니다.
- 각 lab에는 script.sh 파일이 있습니다. 파일 안에는 실습 시 필요한 실행 명령 목록이 있습니다. yaml을 수정 후 이 명령을 복사하여 진행 하시면 됩니다.


