######################################################
# PART 1 : Prometheus 설치하기 
######################################################
cd lab-01

#-------------------------------------------------
# 1. namespace  생성하기 
#-------------------------------------------------
kubectl apply -f 1.namespace.yml

# 확인하기 
kubectl get namespace 

#-------------------------------------------------
# 2. app 설치하기 
#-------------------------------------------------
kubectl apply -f 2.prometheus-deployment.yml

# 확인하기
kubectl get deployment -n lab99
kubectl get configmap -n lab99

#-------------------------------------------------
# 3. 서비스로 노출하기 
#-------------------------------------------------
kubectl apply -f 3.prometheus-service.yml

# 확인하기 
kubectl get svc -n lab99
