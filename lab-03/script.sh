######################################################
# PART 3 : grafana 설치하기 
######################################################
cd lab-03

#-------------------------------------------------
# 1. app 설치하기 
#-------------------------------------------------
kubectl delete -f 1.grafana-deployment.yml
kubectl apply -f 1.grafana-deployment.yml

# 확인하기 
kubectl get deployment -n lab99
kubectl get configmap -n lab99
kubectl get pod -n lab99

#-------------------------------------------------
# 2. 서비스로 노출하기 
#-------------------------------------------------
kubectl apply -f 2.grafana-service.yml

# 확인하기 
kubectl get svc -n lab99


######################################################
# IKS Storagecalss 사용하여 앱 설치하기
# 주의) Storageclass 사용 비용 발생함
######################################################
# kubectl delete -f 1.grafana-deployment.yml

# 1. pv 생성
# 생성되는데 시간이 좀 오래 걸림
# storageclass는  gold, silver, bronze 등급으로 나뉘어 있음 
# kubectl apply -f 4.pvm.yml

# 2. app 설치하기 
# kubectl apply -f 5.grafana-deployment.yml

helm install stable/grafana --version 5.0.0