


# 2. add helm repo
helm repo add kubernetes https://kubernetes-charts.storage.googleapis.com

# 3. intall
# https://cloud.ibm.com/kubernetes/helm/kubernetes/prometheus
helm install -f 0.prom-helm.yml --name my-prom stable/prometheus --namespace lab-choi

# 4.수정 후 업데이트
# https://gruuuuu.github.io/cloud/l-helm-basic/#
 helm upgrade -f 0.prom-helm.yml my-prom stable/prometheus

# 5. service 노출하기 

# 6. helm delete
helm delete my-prom --purge

# 전체 삭제
kubectl delete namespace lab99


# install client helm
helm version 
curl -L https://git.io/get_helm.sh | bash -s -- --version v2.12.3

helm repo update

# iks 1.17.11_1537
# helm version 을 올려야 함 
curl -L https://git.io/get_helm.sh | bash -s -- --version v2.16.5



# helm client vs server vesion 다를 경우
helm reset --force # server tiller 삭제
helm init --upgrade # client version 이랑 server랑 동일하게 맞춤
