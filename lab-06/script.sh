cd lab-06

# helm client vs server vesion 다를 경우
helm reset --force # server tiller 삭제
helm init --upgrade # client version 이랑 server랑 동일하게 맞춤


# 1. app 설치하기 
helm delete lab99-prom --purge
helm install -f 0.prom-helm.yml  \
    --name lab99-prom stable/prometheus   \
    --namespace lab99

# prometheus를 같은 namespace에 생성하면 이름이 충돌나서 안됨, 다른 네임스페이스에 생성할 것
helm delete m2m-private-prom --purge
helm install -f 0.prom-helm.yml  \
    --name m2m-private-prom stable/prometheus   \
    --namespace m2m-private

# 확인하기 
kubectl get deployment -n lab99
kubectl get configmap -n lab99
kubectl get pod -n lab99
kubectl get svc -n lab99


# 권한 문제
tiller에 clusterrolebinding을 해 주어야 함
tiller-deployment에 serviceaccount를 생성해서 할당해 주어야 함

1. service account 생성
2. tiller-deployment spec에 serviceAccountName: tiller or serviceAccount: tiller  추가해줌
3. clusterrolebinding에 생성한 serviceaccount를 넣고 생성함

# 1.service account
kubectl create serviceaccount --namespace kube-system tiller
# 2. tiller deployment 수정 
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
# 3. clusterrolebinding 생성 
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller


https://nextgencloud@gitlab.com/jenkins28/mvp-jenkins-springboot-sample.git

git clone 
mvn clean install
docker build --tag docker.io/nextgencloud/springboot-mvp:1.0 .
docker push docker.io/nextgencloud/springboot-mvp:1.0
docker tag docker.io/nextgencloud/springboot-mvp:1.0 docker.io/nextgencloud/springboot-mvp:latest
docker push docker.io/nextgencloud/springboot-mvp:latest