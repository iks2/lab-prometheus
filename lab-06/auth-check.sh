#!/bin/bash 
# 사용법
# ./auth-check.sh [namespace 입력]
echo "${1}'s namespace is selected."
# For v1.8.x default policy, this 'curl' results in '403 Forbidden' 
APISERVER=$(kubectl config view | grep server | cut -f 2- -d ":" | tr -d " ") 
echo $APISERVER
# Retrieve 'prometheus-sa' account's TOKEN in 'lab-choi' namespace 
TOKEN="$(kubectl get secret -n ${1} $(kubectl get secrets -n ${1} | grep my-prom-prometheus-server | cut -f1 -d ' ') -o jsonpath='{$.data.token}' | base64 --decode)" 
echo $TOKEN
curl -D - --insecure --header "Authorization: Bearer $TOKEN" $APISERVER/metrics
